type Name = String
type Category = String
type Price = Double
type Item = (Name, Category, Price)

getName :: Item -> Name
getName (name, _, _) = name

getCategory :: Item -> Category
getCategory (_, category, _) = category

getPrice :: Item -> Price
getPrice (_, _, price) = price

getPriceV2 :: (Price, Item) -> Price
getPriceV2 (price, _) = price

getItem :: (Price, Item) -> Item
getItem (_, item) = item

unique :: [Name] -> [Name] -> [Name]
unique [] l = l
unique (x:xs) l = if (length (filter (\f -> f == x) l) > 0) then unique xs l else unique xs (x:l)

sumShop :: [Item] -> Price -> Price
sumShop [] res = res
sumShop (x:xs) res = sumShop xs ((getPrice x) + res)


calculate :: [Item] -> [(Name, Price, Category)] -- [(Name, Price, [(Price, Item)])]
calculate [] = []
calculate l = let
        shops = unique (map getName l) []
        shopsMap = map (\shopName -> let
            shopItems = filter (\n -> shopName == getName n) l
            average = (sumShop shopItems 0) / fromIntegral (length shopItems)
            calcAvrList = map (\z -> ((abs (average - getPrice z)), z)) shopItems
            minVal = minimum (map (\h -> getPriceV2 h) calcAvrList)
            in (shopName, average, getCategory (getItem (head (filter (\o -> minVal == getPriceV2 o) calcAvrList))))) shops
        
    in shopsMap