#lang racket
;; Petar Toshev, fn. 81835, CS, gr. 1
;; Var. Б

;; Task 1 a)
(define (sum-digit-divisors n)
  (define (hlp x res)
    (define last_digit (modulo x 10))
    (cond ((< x 1) res)
          ((and (not (zero? last_digit)) (zero? (modulo n last_digit))) (hlp (floor (/ x 10)) (+ res (modulo x 10))))
          (else (hlp (floor (/ x 10)) res))
    )
  )
  (hlp n 0)
)


;(sum-digit-divisors 24)
;(sum-digit-divisors 23)
;(sum-digit-divisors 128)
;(sum-digit-divisors 31)
;(sum-digit-divisors 42)


;; Task 1 b)
(define (from-to a b) (if (> a b) '() (cons a (from-to (+ a 1) b))))
;(from-to 28 35)

(define (same-sum a b)
  (define (map-sum a b) (map (lambda (x) (sum-digit-divisors x)) (from-to a b)))
  (define (count-same-list l)
    (define (count-same n l)
      (cond ((null? l) 0)
            ((not (= (car l) n)) (count-same n (cdr l)))
            (else (+ 1 (count-same n (cdr l))))
      )
    )
    (define (hlp n remaining res)
      (define curr-n (count-same n l))
      (if (zero? remaining)
          res
          (hlp (+ n 1) (- remaining curr-n) (+ res (/ (* curr-n (- curr-n 1)) 2)))
      )
    )
    (hlp 0 (length l) 0)
  )
  (count-same-list (map-sum a b))
)
;(same-sum 28 35)

;; Task 2
(define (best-metric? ml ll)
  (define (calc-metric-on-lists m ll) (if (null? ll) ll (cons (m (car ll)) (calc-metric-on-lists m (cdr ll)))))
  (define (genetare-metrics-results ml) (if (null? ml) ml (cons (calc-metric-on-lists (car ml) ll) (genetare-metrics-results (cdr ml)))))
  (define (generate-id ll)
    (define (hlp ll id) (if (null? ll) ll (cons (cons id (car ll)) (hlp (cdr ll) (+ id 1)))))
    (hlp ll 0)
  )
  ;; lmlr - list of metrics list results
  (define (check lmlr)
    (define (compare-mlr a b)
      (define (compare-hlp a b) (if (null? a) #t (and (> (car a) (car b)) (compare-hlp (cdr a) (cdr b)))))
      (if (= (car a) (car b))
          #t
          (compare-hlp (cdr a) (cdr b))
      )
    )
    (define (check-rec-curr-to-all curr lmlr) ;; iterates through all and compares via compare-mlr
      (if (null? lmlr) #t (and (compare-mlr curr (car lmlr)) (check-rec-curr-to-all curr (cdr lmlr))))
    )
    (define (check-rec-all lmlr-hlp) ;; sets curr for check-rec-curr-to-all
      (if (null? lmlr-hlp) #f (or (check-rec-curr-to-all (car lmlr-hlp) lmlr) (check-rec-all (cdr lmlr-hlp))))
    )
    (check-rec-all lmlr)
  )
  (check (generate-id (genetare-metrics-results ml)))
)

;(define (prod l) (apply * l))
;(define (sum l) (apply + l))
;(best-metric? (list sum prod) '((0 1 2) (3 -4 5) (1337 0)))
;(best-metric? (list car prod) '((100 -100) (29 1) (42)))



;; Task 3
(define (deep-delete l)
  (define (hlp l n)
    (cond ((null? l) l)
          ((list? (car l)) (cons (hlp (car l) (+ 1 n)) (hlp (cdr l) n)))
          ((< (car l) n) (hlp (cdr l) n))
          (else (cons (car l) (hlp (cdr l) n)))
    )
  )
  (hlp l 1)
)

;(deep-delete '(1 (2 (2 4) 1) 0 (3 (1 ))))
;(deep-delete '(1 (2 (2 4 5 0) 1) 0 (3 (1 (7 3)))))
