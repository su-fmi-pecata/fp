prt :: String
prt = "Hello, Worls!aaaa"

fact :: Int -> Int
fact n
    | n == 0    = 1
    | n > 0     = n * fact (n-1)
    | otherwise = error "aaaaaa"
    
fibonacci :: Int -> Int
fibonacci n
    | n == 0 = 1
    | n > 0  = n + fibonacci n-1
    | otherwise = error "aaaaaa"


type Name = String
type Hour = Int
type Minute = Int
type Time = (Hour, Minute)
type Duration = Int
type TvShow = (Name, Time, Duration)


shows :: [TvShow]
shows = [("A", (11, 0), 120),
         ("B", (12, 0), 15),
         ("C", (10, 30), 90)]


getName :: TvShow -> Name
getName (name, _, _) = name

getTime :: TvShow -> Time
getTime (_, time, _) = time

getHour :: TvShow -> Time
getHour (hour, _) = hour


lastShow :: [TvShow] -> Name
lastShow l
    | l == [] = error "No shows"
    | length l == 1 = getName (head l)
-- --lastShow :: [Shows] -> String
-- lastShow l
--     | null l         = "oopa"
--     | length l == 1  = name (head l) 
--     | otherwise = "aaaaaa"