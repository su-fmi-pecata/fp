-- ################################################
-- ## 00. (2p.) group
-- ################################################
group :: Eq a => [a] -> [[a]]
group = groupBy (==)

-- ################################################
-- ## 01. (1p.) sortBy
-- ################################################

mergeSorted :: (a -> a -> Ordering) -> [a] -> [a] -> [a]
mergeSorted cmp l  [] = l
mergeSorted cmp [] r  = r
mergeSorted cmp l@(l1:l2) r@(r1:r2)
    | GT == cmp l1 r1 = r1 : mergeSorted cmp l  r2
    | otherwise       = l1 : mergeSorted cmp l2 r

mergeSortHlp :: (a -> a -> Ordering) -> [a] -> Int -> [a]
mergeSortHlp cmp [] len = []
mergeSortHlp cmp l  1   = l
mergeSortHlp cmp l  len = mergeSorted cmp 
                                      (mergeSortHlp cmp (take len2 l) len2)
                                      (mergeSortHlp cmp (drop len2 l) (len-len2))
                                      where len2 = (len `div` 2) :: Int

mergeSort :: (a -> a -> Ordering) -> [a] -> [a]
mergeSort cmp l = mergeSortHlp cmp l $ length l

sortBy :: (a -> a -> Ordering) -> [a] -> [a]
sortBy = mergeSort

-- ################################################
-- ## 02. (1p.) groupBy
-- ################################################derive

groupByHlp :: (a -> a -> Bool) -> [a] -> [[a]] -> [[a]]
groupByHlp p []    r  = last r : reverse (map reverse $ init r)
groupByHlp p (f:l) [] = groupByHlp p l [[f]]
groupByHlp p (f:l) res@(first@(c:b):r)
    | p f c     = groupByHlp p l [f:first] ++ r
    | otherwise = groupByHlp p l [[f]] ++ res

groupBy :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy p [] = []
groupBy p l  = groupByHlp p (reverse l) []

-- ################################################
-- ## 03.00 (0p.) on
-- ################################################

on :: (b -> b -> c) -> (a -> b) -> a -> a -> c
on f t x y = f (t x) (t y)

-- ################################################
-- ## 03.01 (0p.) &&&
-- ################################################

(&&&) :: (a -> b) -> (a -> c) -> a -> (b, c)
(&&&) f1 f2 x = (f1 x, f2 x)

-- ################################################
-- ## 04. (2p.) sortOn
-- ################################################

sortOn :: Ord b => (a -> b) -> [a] -> [a]
sortOn f l = map snd $ sortBy (compare `on` fst) $ map (f &&& id) l

-- ################################################
-- ## 05. (2p.) groupOn
-- ################################################

groupOn :: Eq b => (a -> b) -> [a] -> [[a]]
groupOn f l = map (map snd) $ groupBy ((==) `on` fst) $ map (f &&& id) l

-- ################################################
-- ## 06. (2p.) classifyOn
-- ################################################

classifyOn :: Ord b => (a -> b) -> [a] -> [[a]]
-- classifyOn f l = map (map snd) $ groupBy ((==) `on` fst) $ sortBy (compare `on` fst) $ map (f &&& id) lmap (map snd) $ groupBy ((==) `on` fst) $ sortBy (compare `on` fst) $ map (f &&& id) l
classifyOn f l = groupOn f $ sortOn f l

-- ################################################
-- ## 07. (5p.) [BONUS] NotEmptyGroups
-- ################################################

data NonEmpty a = a :| [a] deriving (Show)

mapNonEmpty :: (a -> b) -> NonEmpty a -> NonEmpty b
mapNonEmpty f (x :| l) = f x :| map f l

convertToNonEmpty :: [a] -> NonEmpty a
convertToNonEmpty (b:c) = b :| c

groupNonEmpty :: Eq a => [a] -> [NonEmpty a]
groupNonEmpty = groupByNonEmpty (==)

groupByNonEmpty :: (a -> a -> Bool) -> [a] -> [NonEmpty a]
groupByNonEmpty f l = map convertToNonEmpty $ groupBy f l

groupOnNonEmpty :: Eq b => (a -> b) -> [a] -> [NonEmpty a]
groupOnNonEmpty f l = map convertToNonEmpty $ groupOn f l

sortOnNonEmpty :: Ord b => (a -> b) -> [a] -> NonEmpty a
sortOnNonEmpty f l = convertToNonEmpty $ sortOn f l

classifyOnNonEmpty :: Ord b => (a -> b) -> [a] -> [NonEmpty a]
classifyOnNonEmpty f l = map convertToNonEmpty $ classifyOn f l