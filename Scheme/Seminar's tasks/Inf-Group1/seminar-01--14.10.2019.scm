(load "../../Generic/prime.scm")
; bad because too many context params in recursion
;(define  (acc val oper curr next cond)
;  (if (cond curr)
;      (acc (oper val curr) oper (next curr) next cond)
;      val
;  )
;)

(define  (acc null oper first next test)
  (define (acc_help val curr) 
    (if (test curr)
        (acc_help (oper val curr) (next curr))
        val
    )
  )
  (acc_help null first)
)

(define (sumPrimes n)
  (define (next n) (+ n 2))
  (define (test x) (<= x n))
  (define (oper val x) (if (prime? x) (+ val x) val))
  (acc 2 oper 3 next test)
)

; is prime via acc
(define (inc x) (+ x 1))

(define (hasDivV1? n)
  (= 2 (countDivV1 n))
)

(define (countDivV1 n)
  (define (oper1 val x) (if (= 0 (modulo n x)) (+ val 1) val))
  (define (test x) (<= x n))
  (acc 1 oper1 2 inc test)
)

; v2

(define (inc2 x) (+ x 2))
(define (hasDivV2? n)
  (and (odd? n) (= 0 (countDivV2 n)))
)
(define (countDivV1 n)
  (define (oper1 val x) (if (= 0 (modulo n x)) (+ val 1) val))
  (define (test x) (<= (* x x) n))
  (acc 0 oper1 3 inc2 test)
)

;(define (isPrime? n)
  
;)
