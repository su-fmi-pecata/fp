#lang racket

;; flatten -> make list flat
(define (flatten l)
  (cond ((null? l) l)
        ((list? (car l)) (append (flatten (car l)) (flatten (cdr l))))
        (else (cons (car l) (flatten (cdr l))))
  )
)

;; (flatten '(1 2 (3 4 (5)) 6))

(define (depth l)
  (define (depth-hlp l result)
    (cond ((null? l) result)
          ((list? (car l)) (max (depth-hlp (car l) (+ 1 result)) (depth-hlp (cdr l) result)))
          (else (depth-hlp (cdr l) result))
    )
  )
  (depth-hlp l 1)
)

;;(depth '(1 2 (3 4 (5)) 6))

(define (sum-list l)
  (define (sum-hlp l res)
    (cond ((null? l) res)
          ((list? (car l)) (+
                            res
                            (sum-hlp (car l) 0)
                            (sum-hlp (cdr l) 0)
                           ))
          (else (sum-hlp (cdr l) (+ (car l) res)))
    )
  )
  (sum-hlp l 0)
)

;;(sum-list '(1 2 (3 4 (5)) 6))
(define (1+ n) (+ 1 n))

(define (map l f)
  (cond ((null? l) l)
        ((list? (car l)) (cons (map (car l) f) (map (cdr l) f)))
        (else (cons (f (car l)) (map (cdr l) f)))
  )
)

;; (map '(1 2 (3 4 (5)) 6) values)
;; (map '(1 2 (3 4 (5)) 6) 1+)

;;
(define (list? l)
  (or (null? l)
      (and (pair? l) (list? (cdr l)))
  )
)
;; (list? '(1 2 (3 4 (5)) 6))
;; (list? (cons 1 (cons 2 3)))

;(define (filter l p)
 ; (cond ((null? l) l)
  ;      ((list? (car l)) (cons (filter (car l) p) (filter (cdr l) p)))
   ;     ((p (car l)) (cons (car l) (filter (cdr l) p)))
    ;    (else (filter (cdr l) p))
  ;)
;)

(define (matrix? l)
  (define (m? l len)
    (or (null? l)
        (and (list? l)
             (list? (car l))
             (= (length (car l)) len)
             (= (depth (car l)) 1)
             (m? (cdr l) len)
        )
    )
  )
  (if (null? l) #f ;; we do not accept matrix with dim 0
      (and (list? l) (list? (car l)) (m? l (length (car l))))
  )
)

(define (matrix? l)
  (define (m? l len)
    (or (null? l)
        (and (list? l)
             (list? (car l))
             (= (length (car l)) len)
             (= (depth (car l)) 1)
             (m? (cdr l) len)
        )
    )
  )
  (if (null? l) #f ;; we do not accept matrix with dim 0
      (and (list? l) (list? (car l)) (m? l (length (car l))))
  )
)

(matrix? '(1 2 (3 4 (5)) 6))
(matrix? '((1 1 1) (2 2 2) (3 3 3)))