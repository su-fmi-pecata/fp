(define (derive f)
  (define d 0.000001)
  (lambda (x)
    (/ (- (f (+ x d)) (f x)) d)
  )
)


(load "../../Generic/accumulate.scm")
(define (integrate f)
  (lambda (a b)
    (accumulate +
                0 a b
                (lambda (x) (/ (+ (f x) (f (+ x o.0001)))))
                (lambda (x) (+ x 0.0001))
    )
  )
)

;; lists

;; functions:
;; (cons x y) -> (x. y) // in middle it has point
;; (car (cons x y)) -> x
;; (cdr (cons x y)) -> y
;; null values: #f '()
;; '() means empty list
;; '(1 2 3 4 5) -> list creation
;; '(+ x y)

(define (sum-num l)
  (if (null? l)
      0
      (+ (car l) (sum-num (cdr l)))
  )
)

;; foldr - recursion list 
;; foldl - iteration
;; take - gets first n element of list
;; drop - skip first n element of list

(define (sum-num2 l)
  (foldr 0 + l)
)

(define (take n l)
  (if (or (= n 0) (null? l))
      '()
      (const (car l) (take (- n 1) (cdr l)))
  )
)


(define (id-list l)
  (foldr '() (lambda (x y) (const y x)) l)
)

(define (map f l)
  (foldr '() (lambda (x y) (const (f y) x)) l)
)

;; (caar l) -> (car (car l))
;; (cadr l) -> (car (cdr l))
;; (cdar l) -> (cdr (car l))
;; (cddr l) -> (cdr (cdr l))

(define (sorted? l)
  (if (or (null? l) (null? (cdr l)))
      (and (<= (car l) (car (cdr l)))
           (sorted? (cdr l))
      )
  )
)

;; 4 different ways of compareing
;; = -> used for aritmethics
;; eq? -> compare if they are same objects in memory
;; eqv? -> ocenkata na elementa se sravnqva
;; equal? -> recursion comparing lists



(define (insert-sorted l x)
  (cond ((null? l) (list x)) ;; (list x) <=> (const x '()) <=> (const x l) // because l is '()
        ((< x (car l)) (cons x l))
        (else (cons (car l) (insert-sorted (cdr l) x)))
  )
)

(define (merge l1 l2)
  (cond ((null? l1) l2)
        ((null? l2) l1)
        ((< (car l1) (car l2)) (cons (car l1)) (merge (cdr l1) l2))
        (else (cons (car l2)) (merge l1 (cdr l2)))
  )
)


;; HW create unzip functon

(define (merge-sort l)
  (if (or (null? l) (null? (cdr l)))
   l
   (let (len2 (quotient (length l) 2))
     (merge (merge-sort (take len2 l))
          (merge-sort (drop len2 l))
     )
   )
  )
)


(define (ins-sort-hlp res l)
  (if (null? l)
      res
      (ins-sort-hlp '() res)
  )
)
(define (ins-sort l)
  (if (or (null? l) (null? (cdr l)))
      l
      (ins-sort-hlp (ins-sort res (car l)) (cdr l)) ;; <=> (foldl '() ins-sort l)
  )
)

;; slow :/
(define (qsort l)
  (if (or (null? l) (null? (cdr l)))
      l
      (append (qsort (filter (lambda (x) (< x (car l))) l))
              (filter (lambda (x) (= x (car l))) l)
              (qsort (filter (lambda (x) (> x (car l))) l))
      )
  )
)


(define (qsort l)
  (if (or (null? l) (null? (cdr l)))
      l
      (append (qsort (filter (lambda (x) (<= x (car l))) (cdr l)))
              (cons (car l) (qsort (filter (lambda (x) (> x (car l))) l)))
      )
  )
)

;; HW buble sort, selection sort, shaker short, etc. NOT BINARY SEARCH
