;; task 1
(define (count-digit d n)
  (if (= n 0)
      0
      (+ (if (= d (modulo n 10)) 1 0) (count-digit d (floor (/ n 10))))
  )
)

(define (test-count-digit)
  (and 
    (equal? 1 (count-digit 2 12))
    (equal? 0 (count-digit 3 12))
    (equal? 2 (count-digit 2 1234567823))
  )
)
"count-digit: " (test-count-digit)

;; task 2
(define (fact n)
  (if (<= n 0)
      1
      (* n (fact (- n 1)))
  )
)

(define (test-fact)
  (and
    (equal? 120 (fact 5))
    (equal? 720 (fact 6))
    (equal? 1 (fact 0))
  )
)
"fact: " (test-fact)

;; task 3
(define (fib n)
  (if (<= n 1)
      1
      (+ (fib (- n 2)) (fib (- n 1)))
  )        
)

(define (test-fib)
  (and
    (equal? 1 (fib 0))
    (equal? 1 (fib 1))
    (equal? 2 (fib 2))
    (equal? 3 (fib 3))
    (equal? 5 (fib 4))
    (equal? 8 (fib 5))
  )
)
"fib: " (test-fib)

;; task 4
(define (reverse-int n)
  (define (calc new old)
    (if (= old 0)
        new
        (calc (+ (* new 10) (modulo old 10)) (floor (/ old 10)))
    )
  )
  (calc 0 n)
)


(define (test-reverse-int)
  (and
    (equal? 5 (reverse-int 5))
    (equal? 1 (reverse-int 1))
    (equal? 1234 (reverse-int 4321))
  )
)
"reverse-int: " (test-reverse-int)

;; skip some tasks because i've already done them

;; task 9
(define (increasing? n)
  
  (if (< n 10)
      #t
      (and
       (> (modulo n 10) (modulo (floor (/ n 10)) 10))
       (increasing? (floor (/ n 10)))
      )
  )
      
)

(define (test-increasing?)
  (and
    (equal? #t (increasing? 5))
    (equal? #t (increasing? 12))
    (equal? #f (increasing? 122))
    (equal? #f (increasing? 4321))
    (equal? #t (increasing? 1234))
  )
)
"increasing?: " (test-increasing?)