(load "../../Generic/accumulate.scm")

(define (1+ n) (+ n 1))


;; Task 1
(define (!! n)
  (filter-accumulate * 1 1 n values 1+ (lambda (x) (= (modulo x 2) (modulo n 2))))
)

;; Task 2
(define (factoriel n)
  (accumulate * 1 2 n values 1+)
)

(define (nchk n k)
  (/ (factoriel n) (* (factoriel (- n k)) (factoriel k)))
)

;; Task 3
(define (nchk* n k)
  (accumulate * 1 1 k
              (lambda (i) (/ (+ n (- k) i) i))
              1+
  )
)

;; Task 4
(define (2^ n) (accumulate * 1 1 n (lambda (a) 2) 1+))

(define (2^ n)
  (accumulate + 0 0 n
              (lambda (i) (nchk* n i))
              1+
  )
)

;; Task 5 - already done somewhere

;; Task 6
(define (count p? a b)
  (filter-accumulate + 0 a b (lambda (x) 1) 1+ p?)
)

;; Task 6.5
(define (or2 a b) (or a b))
(define (and2 a b) (and a b))

(define (all? p? a b)
  (accumulate and2 #t a b (lambda (x) (p? x)) 1+)
)

(define (any? p? a b)
  (accumulate or2 #f a b (lambda (x) (p? x)) 1+)
)

;; Task 7 - already done

;; Task 8
(define (constantly c) (lambda (x) c))

;; Task 9
(define (flip f) (lambda (a b) (f b a)))

;; Task 10
(define (complement p) (lambda (x) (not (p x))))

;; Task 11
(define (twist k f g)
  (if (= k 0)
      values
      (lambda (x) (f (g ((twist (- k 2) f g) x))))
  )
)

;; Task 12
(define (permutable? a b f g)
  (filter-accumulate + 0 a b (lambda (x) (if (= ((twist x f g)x) ((twist x g f)x)) 1 0)) 1+ even?)
)


(define (permutable? a b f g)
  (accumulate + 0 a b
              (lambda (x) (if (or (odd? x) (= ((twist x f g)x) ((twist x g f)x))) 1 0))
              1+)
)

;; might be best solution..?
(define (permutable? a b f g)
  (all? (lambda (k) (or (odd? k) (= ((twist k f g) k) ((twist k g f) k)))) a b)
)

;; Task 13
(define (sort-digits n)
  
)