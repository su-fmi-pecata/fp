(define (_gcd a b)
  (cond ((= a b) a)
      ((> a b) (_gcd (- a b) b))
      (else (_gcd a (- b a)))
  )   
)

(_gcd 3 5)
(_gcd 2 4)
(_gcd 2 27)
(_gcd 2 28)
(_gcd 3 27)

;; obrazuva se lineen iterativen proccess