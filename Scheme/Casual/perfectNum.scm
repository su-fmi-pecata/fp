(define  (acc null oper first next test)
  (define (acc_help val curr) 
    (if (test curr)
        (acc_help (oper val curr) (next curr))
        val
    )
  )
  (acc_help null first)
)

(define (sum a b) (+ a b))
(define (inc a) (+ a 1))

(define (perfect? n)
  (define (test a) (< a n))
  (define (sumDiv val curr) (if (= 0 (modulo n curr)) (+ val curr) val))
  (= n (acc 0 sumDiv 1 inc test))
)

(define (sumPerfectTo n)
  (define (test a) (<= a n))
  (define (sumPerf val x) (if (perfect? x) (sum val x) val))
  (acc 0 sumPerf 2 inc test)
)


;; HW function to calc f(x)'
;;Integrate f in [a,b]