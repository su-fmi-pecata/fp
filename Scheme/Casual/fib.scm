(define (fib_default n)
  (if (< n 2)
      1
      (+ (fib_default (- n 1)) (fib_default (- n 2)))
  )
)

(define (fib_fast_calc number index prev curr)
  (if (= index number)
      (+ prev curr)
      (fib_fast_calc number (+ index 1) curr (+ prev curr))
  )
)


(define (fib_fast n)
  (if (< n 2)
      1
      (fib_fast_calc n 2 1 1)
  )
)


(define (fib n)
  (define (fib_optimal number prev curr)
    (if (< number 0)
        curr
        (fib_optimal (- number 1) curr (+ prev curr))
    )
  )
  (if (> n 2)
      (fib_optimal (- n 2) 1 1)
      1
  )
)

(fib 13)
(fib_fast 13)
(= (fib 13) (fib_fast 13))