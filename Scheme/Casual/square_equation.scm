;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname square_equation) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
(define (square_equation a b c)
  (if (= a 0)
      (/ (- c) b)
      (/ (+ (- b) (sqrt (- (* b b) (* 4 a c)))) (* 2 a))
  )
)