(define (prime? x)
  (define (check d)
    (if (>= x (* d d))
        (and (not (= 0 (modulo x d))) (check (+ d 2) ))
        #t
    )
  )
  (or
   (= x 2)
   (and
    (> x 2)
    (odd? x)
    (check 3)
   )
  )
)