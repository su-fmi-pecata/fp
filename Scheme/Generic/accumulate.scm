(define (accumulate-abstract
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
           p?   ;; predicateFunction
         )
  (cond
    ((> a b) nv)
    ((p? a) (op (term a) (accumulate-abstract op nv (next a) b term next p?)))
    (else (accumulate-abstract op nv (next a) b term next p?))
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ACCUMULATE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (accumulate-old
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
         )
  (if (> a b)
      nv
      (op (term a) (accumilate op nv (next a) b term next))
  )
)

(define (accumulate
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
         )
  (accumulate-abstract op nv a b term next (lambda (a) #t))
)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ACCUMULATE-I
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (accumulate-i-old
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
         )
  (if (> a b)
      nv
      (op (accumilate-i op nv (next a) b term next) (term a))
  )
)

(define (accumulate-i
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
         )
  (accumulate-abstract
   (lambda (termCalc nextAccumulate) (op nextAccumulate termCalc))
   nv a b term next
   (lambda (a) #t)
  )
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FILTER ACCUMULATE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (filter-accumulate-old
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
           p?   ;; predicate
         )
  (cond
    ((> a b) nv)
    ((p? a) (op (term a) (filter-accumulate-old op nv (next a) b term next p?)))
    (else (filter-accumulate-old op nv (next a) b term next p?))
  )
)


(define (filter-accumulate
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
           p?   ;; predicate
         )
  (accumulate-abstract op nv a b term next p?)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FILTER ACCUMULATE-I
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (filter-accumulate-i-old
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
           p?   ;; predicate
         )
  (cond
    ((> a b) nv)
    ((p? a) (op (filter-accumulate-i-old op nv (next a) b term next p?) (term a)))
    (else (filter-accumulate-i-old op nv (next a) b term next p?))
  )
)


(define (filter-accumulate-i
           op   ;; operation
           nv   ;; nullValue
           a    ;; startValue
           b    ;; endValue
           term ;; termFunction
           next ;; nextFunxtion
           p?   ;; predicate
         )
  (accumulate-abstract (lambda (termCalc nextAccumulate) (op nextAccumulate termCalc)) nv a b term next p?)
)