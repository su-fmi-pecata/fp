(load "../Generic/prime.scm")

(define (find_first_triangle_number_with_over_500_div)
  
  (define (check x sum)
    (define sqrts (floor (sqrt sum)))
    (define (div_num div n)
      (if (<= div sqrts)
          (div_num (+ div 1) (+ n (if (zero? (modulo sum div)) 1 0)))
          (- (* 2 n) (if (= sum (* sqrts sqrts)) 1 0))
      )
    )
    (if (and (not (prime? sum)) (<= 500 (div_num 1 0)))
        x
        (check (+ x 1) (+ sum x 1))
    )
  )
  (check 1 1)
)

(find_first_triangle_number_with_over_500_div)