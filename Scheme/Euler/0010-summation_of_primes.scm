(load "../Generic/prime.scm")

(define (sum_of_primes_below n)
  (define (f2 n)
    (if (<= n 2)
        2
        (+ (if (prime? n) n 0) (f2 (- n 1)))
    )
  )
  (f2 (+ (- n 1) (modulo n 2)))
)


(define sum_of_primes_below_10 (sum_of_primes_below 10))
sum_of_primes_below_10
(= 17 sum_of_primes_below_10)
(define sum_of_primes_below_2000000 (sum_of_primes_below 2000000))
sum_of_primes_below_2000000