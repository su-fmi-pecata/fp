(define (sum n)
  (define (calc n)
    (if (<= n 0)
        0
        (+ (if (or (= 0 (modulo n 3)) (= 0 (modulo n 5))) n 0) (calc (- n 1)))
    )
  )
  (calc (- n 1))
)

