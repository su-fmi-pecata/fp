(load "../Generic/prime.scm")

(define (find_prime n)
  (define calc_step 2)
  (define (calc n x)
    (if (= n 0)
        (- x calc_step)
        (calc (- n (if (prime? x) 1 0)) (+ x calc_step))
    )
  )
  (if (= n 1)
      2
      (calc (- n 1) 3)
  )
)

(define find_prime_6 (find_prime 6))
find_prime_6
(= 13 find_prime_6)
(define find_prime_10001 (find_prime 10001))
find_prime_10001
(= 104743 find_prime_10001)